import { useEffect } from 'react';

const ReplainChatWidget = () => {
  useEffect(() => {
    window.replainSettings = { id: 'fa10e87e-64d4-4992-bd60-37a375524ba2' };
    (function(u){var s=document.createElement('script');s.async=true;s.src=u;
    var x=document.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);
    })('https://widget.replain.cc/dist/client.js');
  }, []);

  return null; // or you can return a placeholder if needed
}

export default ReplainChatWidget;
