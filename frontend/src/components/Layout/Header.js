import React from "react";
import {useNavigate} from "react-router-dom";

export default function Header() {
    const navigate = useNavigate();

    const handleLogout = () => {
        // Clear user data from local storage or state
        localStorage.removeItem("accessToken");
        localStorage.removeItem("refreshToken");

        // Redirect the user to the login page
        navigate("/auth/signin");
    };

    return (
        <div
            className="d-flex justify-content-between rounded-3 p-3 m-3"
            style={{backgroundColor: "#ffff"}}
        >
            <h2>Online Clinic</h2>
            <button className="btn btn-danger align-self-center" onClick={handleLogout}>
                Log Out
            </button>
        </div>
    );
}
