import React, {useEffect, useState} from "react";
import {FaRegEye, FaRegEyeSlash} from "react-icons/fa6";
import {ToastContainer, toast} from "react-toastify";
import {useNavigate} from "react-router-dom";

import Loader from "../../common/Loader";

export default function SignIn() {
    const navigate = useNavigate();

    const [isLoading, setIsLoading] = useState(true);
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [passwordVisibility, setPasswordVisibility] = useState(false);

    // toggling password visibility
    const togglePasswordVisibility = () => {
        setPasswordVisibility((prev) => !prev);
    };

    // notify user
    const notify = (message, type) =>
        toast[type](message, {
            position: "top-center",
            autoClose: 3000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "light",
        });

    // submit form
    const handleSubmit = async (e) => {
        e.preventDefault();

        try {
            const response = await fetch("http://127.0.0.1:8000/api/login/", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({
                    username,
                    password,
                }),
            });

            const data = await response.json();
            if (response.ok) {
                const {access, refresh} = data;

                // Save tokens to localStorage
                localStorage.setItem("accessToken", access);
                localStorage.setItem("refreshToken", refresh);
                localStorage.setItem("username", username);

                // Fetch user details to get user ID
                const userResponse = await fetch(`http://127.0.0.1:8000/api/user/${username}`);
                const userData = await userResponse.json();
                if (userResponse.ok) {
                    // Save user ID to local storage
                    localStorage.setItem("userId", userData.id);
                }

                notify("Successfully signed in!", "success");
                setTimeout(() => {
                    if (userData.role === "patient") {
                        navigate(`/dashboard/patient/${username}`);
                    } else if (userData.role === "doctor") {
                        navigate(`/dashboard/doctor/${username}`);
                    } else if (userData.role === "admin") {
                        navigate(`/dashboard/admin/${username}`)
                    } else if (userData.role === "lab_tech") {
                        navigate(`/dashboard/lab/${username}`)
                    } else if(userData.role === "nurse"){
                        navigate(`/dashboard/nurse/${username}`)
                    }
                }, 500);
            } else {
                notify("Invalid input!", "error");
            }

            // Clearing inputs
            setPassword("");
            setUsername("");
        } catch (error) {
            console.error("Error:", error);
            notify("Something went wrong!", "error");
        }
    };

    // useEffect to fetch user data and navigate to dashboard if tokens are already stored
    useEffect(() => {
        const fetchData = async () => {
            try {
                setTimeout(() => setIsLoading(false), 500);
                const accessToken = localStorage.getItem("accessToken");
                const refreshToken = localStorage.getItem("refreshToken");
                const username = localStorage.getItem("username");
                const response = (await fetch(`http://127.0.0.1:8000/api/user/${username}/`));
                const userData = await response.json();
                if (accessToken && refreshToken && username) {
                    if (userData.role === "patient") {
                        navigate(`/dashboard/patient/${username}`);
                    } else if (userData.role === "doctor") {
                        navigate(`/dashboard/doctor/${username}`);
                    } else if (userData.role === "admin") {
                        navigate(`/dashboard/admin/${username}`)
                    } else if (userData.role === "lab_tech") {
                        navigate(`/dashboard/lab/${username}`)
                    } else if(userData.role === "nurse"){
                        navigate(`/dashboard/nurse/${username}`)
                    }
                }
            } catch (error) {
                console.error("Error:", error);
                notify("Something went wrong!", "error");
            }
        };

        fetchData();

    }, [navigate, username]); // Add navigate and username to the dependency array

    if (isLoading) {
        return <Loader/>;
    }

    return (
        <div className="d-flex flex-column justify-content-center align-items-center p-5 gap-5">
            <ToastContainer
                position="top-center"
                autoClose={3000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="light"
            />
            <h2 className="text-danger">Sign In</h2>
            <form onSubmit={handleSubmit} className="d-flex justify-content-center row gap-3">
                <div className="d-flex flex-column gap-2 col-12 col-md-6">
                    <label htmlFor="username">Username:</label>
                    <input
                        className="form-control"
                        id="username"
                        type="text"
                        value={username}
                        onChange={(e) => setUsername(e.target.value)}
                    />
                </div>
                <div className="d-flex flex-column gap-2 col-12 col-md-6">
                    <label htmlFor="password">Password:</label>
                    <div style={{position: "relative"}}>
                        <input
                            className="form-control"
                            id="password"
                            type={passwordVisibility ? "text" : "password"}
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                        />
                        {!passwordVisibility ? (
                            <FaRegEye
                                style={{
                                    position: "absolute",
                                    top: "50%",
                                    right: "10px",
                                    cursor: "pointer",
                                    transform: "translateY(-50%)"
                                }}
                                onClick={togglePasswordVisibility}
                            />
                        ) : (
                            <FaRegEyeSlash
                                style={{
                                    position: "absolute",
                                    top: "50%",
                                    right: "10px",
                                    cursor: "pointer",
                                    transform: "translateY(-50%)"
                                }}
                                onClick={togglePasswordVisibility}
                            />
                        )}
                    </div>
                </div>
                <button className="col-12 col-md-6 btn btn-danger mt-5" type="submit">
                    Sign In
                </button>
            </form>
        </div>
    );
}
