import React, { useEffect, useState } from "react";
import { toast, ToastContainer } from "react-toastify";
import { FaRegEye, FaRegEyeSlash } from "react-icons/fa6";
import { useNavigate } from "react-router-dom";
import Loader from "../../common/Loader";

function SignUp() {
    const [isLoading, setIsLoading] = useState(true);
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [passwordVisibility, setPasswordVisibility] = useState(false);
    const navigate = useNavigate();

    useEffect(() => {
        setTimeout(() => setIsLoading(false), 1800);
    }, []);

    const togglePasswordVisibility = () => {
        setPasswordVisibility(prev => !prev);
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const response = await fetch("http://127.0.0.1:8000/api/patient/signup/", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({ username, password }),
            });
            if (response.ok) {
                const responseData = await response.json();
                const accessToken = responseData.access;

                localStorage.setItem("accessToken", accessToken);

                const userResponse = await fetch(`http://127.0.0.1:8000/api/user/${username}`);
                const userData = await userResponse.json();
                if (userResponse.ok) {
                    localStorage.setItem("userId", userData.id);
                    localStorage.setItem("username", username);
                    navigate(`/dashboard/patient/${username}`);
                } else {
                    throw new Error("Failed to fetch user details");
                }
            } else {
                throw new Error("Failed to sign up");
            }
        } catch (error) {
            console.error(error);
            toast.error("Failed to sign up. Please try again later.");
        }
    };

    if (isLoading) {
        return <Loader />;
    }

    return (
        <div className="d-flex flex-column justify-content-center align-items-center p-5 gap-5">
            <ToastContainer
                position="top-center"
                autoClose={3000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="light"
            />
            <div className="d-flex flex-column align-items-center gap-3">
                <h2 className="text-danger">Sign Up</h2>
                <div>
                    <span className="text-bg-danger">Note:</span>{" "}
                    <span> Only Patients can sign up!</span>
                </div>
            </div>
            <form onSubmit={handleSubmit} className="d-flex justify-content-center row gap-3">
                <div className="d-flex flex-column gap-2 col-12 col-md-6">
                    <label htmlFor="username">Username:</label>
                    <input
                        className="form-control"
                        id="username"
                        type="text"
                        value={username}
                        onChange={(e) => setUsername(e.target.value)}
                    />
                </div>
                <div className="d-flex flex-column gap-2 col-12 col-md-6">
                    <label htmlFor="password">Password:</label>
                    <div style={{ position: "relative" }}>
                        <input
                            className="form-control"
                            id="password"
                            type={passwordVisibility ? "text" : "password"}
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                        />
                        {!passwordVisibility ? (
                            <FaRegEye
                                style={{ position: "absolute", top: "50%", right: "10px", cursor: "pointer", transform: "translateY(-50%)" }}
                                onClick={togglePasswordVisibility}
                            />
                        ) : (
                            <FaRegEyeSlash
                                style={{ position: "absolute", top: "50%", right: "10px", cursor: "pointer", transform: "translateY(-50%)" }}
                                onClick={togglePasswordVisibility}
                            />
                        )}
                    </div>
                </div>
                <div className="d-flex flex-column gap-2 col-12 col-md-6">
                    <label>Role:</label>
                    <select className="form-select" value="patient" disabled>
                        <option value="patient">Patient</option>
                    </select>
                </div>
                <button className="col-12 col-md-6 btn btn-danger mt-5" type="submit">
                    Sign Up
                </button>
            </form>
        </div>
    );
}

export default SignUp;
