import React, { useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";

export default function Auth() {
    const navigate = useNavigate();

    useEffect(() => {
        const fetchData = async () => {
            const accessToken = localStorage.getItem("accessToken");
            const refreshToken = localStorage.getItem("refreshToken");
            const username = localStorage.getItem("username");

            if (accessToken && refreshToken && username) {
                try {
                    const response = await fetch(`http://127.0.0.1:8000/api/user/${username}/`);
                    const userData = await response.json();

                    if (userData.role === "patient") {
                        navigate(`/dashboard/patient/${username}`);
                    } else if (userData.role === "doctor") {
                        navigate(`/dashboard/doctor/${username}`);
                    } else if (userData.role === "admin") {
                        navigate(`/dashboard/admin/${username}`);
                    } else if (userData.role === "lab_tech") {
                        navigate(`/dashboard/lab/${username}`);
                    } else if (userData.role === "nurse") {
                        navigate(`/dashboard/nurse/${username}`);
                    }
                } catch (error) {
                    console.error("Error fetching user data:", error);
                    // Handle error, e.g., redirect to sign-in page
                    navigate("/auth/signin");
                }
            }
        };

        fetchData();
    }, [navigate]);

    const handleSignUp = () => {
        navigate("/auth/signup");
    };

    return (
        <div className="d-flex flex-column justify-content-center align-items-center gap-4">
            <h1 className="text-center">Welcome to Online Clinic</h1>
            <div className="d-flex flex-column gap-2">
                <button className="btn btn-danger" onClick={handleSignUp}>
                    Sign Up
                </button>
                <span>
                    Do you already have an account? Please{" "}
                    <Link to="/auth/signin" className="text-danger">
                        sign in
                    </Link>
                </span>
            </div>
        </div>
    );
}
