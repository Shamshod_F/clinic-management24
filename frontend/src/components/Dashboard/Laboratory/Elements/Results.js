// Results.js
import './Results.css'; // Assuming your CSS file is named Results.css

export default function Results({labResults, onSendLabResult}) {
    const TEST_TYPE_CHOICES = [
        ['basic-metabolic', 'Basic Metabolic Panel'],
        ['comprehensive-metabolic', 'Comprehensive Metabolic Panel'],
        ['lipid-profile', 'Lipid profile'],
        ['pregnancy-test', 'Pregnancy test'],
        ['urinalysis', 'Urinalysis'],
    ];
    const testTypeChoicesMap = Object.fromEntries(TEST_TYPE_CHOICES);

    return (
        <div>
            <table className="table table-container">
                <thead>
                <tr>
                    <th>Patient Name</th>
                    <th>Contact Info</th>
                    <th>Test Type</th>
                    <th>Status</th>
                    <th>Date</th>
                    <th>Time</th>
                </tr>
                </thead>
                <tbody>
                {labResults.map(result => (
                    <tr key={result.id} className={result.status === 'positive' ? 'positive' : 'negative'}>
                        <td>{result.lab.patient_name}</td>
                        <td>{result.lab.contact_info}</td>
                        <td>{testTypeChoicesMap[result.lab.test_type]}</td>
                        <td>{result.status}</td>
                        <td>{result.date}</td>
                        <td>{result.time.substring(0, 5)}</td>
                    </tr>
                ))}
                </tbody>
            </table>
        </div>
    );
}
