import React, { useState, useEffect } from "react";
import { IoIosSend } from "react-icons/io";
import { toast } from "react-toastify";

export default function Requests({ appointments }) {
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [selectedAppointment, setSelectedAppointment] = useState(null);
    const [isLoading, setIsLoading] = useState(false);

    const notify = (message, type) =>
        toast[type](message, {
            position: "top-center",
            zIndex: 2000,
            autoClose: 3000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "light",
        });

    useEffect(() => {
        if (selectedAppointment !== null) {
            setIsModalOpen(true);
        }
    }, [selectedAppointment]);

    const toggleModal = (appointment) => {
        console.log("Toggling modal...");
        setSelectedAppointment(appointment);
    };

    const handleOptionClick = (option) => {
        const token = localStorage.getItem("accessToken");
        fetch(`http://127.0.0.1:8000/api/laboratories/${selectedAppointment.id}/`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                status: false,
            })
        })
        .then(response => {
            if (response.ok) {
                // Notify user that the test was sent successfully
                notify("Test sent successfully!", "success");

                // Then, create a LabResult
                fetch('http://127.0.0.1:8000/api/lab-result-new/', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${token}`
                    },
                    body: JSON.stringify({
                        lab: selectedAppointment.id,
                        status: option
                    })
                })
                .then(response => {
                    if (response.ok) {
                        console.log(`LabResult created with ${option} status`);
                        setIsLoading(true);
                    } else {
                        console.error('Failed to create LabResult');
                    }
                })
                .catch(error => {
                    console.error('Error creating LabResult:', error);
                });

                // Close the modal
                setIsModalOpen(false);
            } else {
                console.error('Failed to update laboratory status');
            }
        })
        .catch(error => {
            console.error('Error updating laboratory status:', error);
        });
    };

    useEffect(() => {
        if (isLoading) {
            fetchAppointments();
        }
    }, [isLoading]);

    const fetchAppointments = async () => {
        try {
            const token = localStorage.getItem("accessToken");
            const response = await fetch("http://127.0.0.1:8000/api/laboratories", {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            });
            if (!response.ok) {
                throw new Error("Failed to fetch appointments");
            }
            // Remove the unused variable
            setIsLoading(false);
        } catch (error) {
            console.error("Error fetching appointments:", error);
            setIsLoading(false);
        }
    };

    console.log(`Modal is ${isModalOpen ? "open" : "closed"}`);

    return (
        <div className="row mt-3">
            <div className="col">
                <div className="table-responsive">
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>Patient Name</th>
                                <th>Contact Info</th>
                                <th>Date</th>
                                <th>Time</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {appointments.map((appointment) => (
                                <tr key={appointment.id}>
                                    <td>{appointment.patient_name}</td>
                                    <td>{appointment.contact_info}</td>
                                    <td>{appointment.date}</td>
                                    <td>{appointment.time}</td>
                                    <td>
                                        <button className="btn btn-primary" onClick={() => toggleModal(appointment)}>
                                            <IoIosSend size={20} />
                                        </button>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
            {/* Modal component */}
            {isModalOpen && (
                <div
                    className="modal-open"
                    style={{
                        position: "fixed",
                        zIndex: 1000,
                        left: 0,
                        top: 0,
                        width: "100%",
                        height: "100%",
                        overflow: "auto",
                        backgroundColor: "rgba(0, 0, 0, 0.7)",
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                    }}
                >
                    <div
                        className="modal-content-open"
                        style={{
                            marginLeft: "100px",
                            backgroundColor: "#fff",
                            borderRadius: "8px",
                            boxShadow: "0 4px 8px rgba(0, 0, 0, 0.1)",
                            padding: "20px",
                            width: "80%",
                            maxWidth: "500px",
                            justifyContent: "center",
                            alignItems: "center",
                            textAlign: "center"
                        }}
                    >
                        <h3>Select Option</h3>
                        <button
                            style={{
                                marginRight: "10px",
                                padding: "10px 20px",
                                borderRadius: "5px",
                                border: "none",
                                backgroundColor: "#5cb85c",
                                color: "#fff",
                                cursor: "pointer",
                            }}
                            onClick={() => handleOptionClick("positive")}
                        >
                            Positive
                        </button>
                        <br />
                        <button
                            style={{
                                marginRight: "10px",
                                marginTop: "10px",
                                padding: "10px 15.3px",
                                borderRadius: "5px",
                                border: "none",
                                backgroundColor: "#d9534f",
                                color: "#fff",
                                cursor: "pointer",
                            }}
                            onClick={() => handleOptionClick("negative")}
                        >
                            Negative
                        </button>
                        <button
                            style={{
                                fontSize: "12px",
                                padding: "7px 15px",
                                borderRadius: "5px",
                                border: "none",
                                marginLeft: "400px",
                                backgroundColor: "#337ab7",
                                color: "#fff",
                                cursor: "pointer",
                            }}
                            onClick={() => setIsModalOpen(false)}
                        >
                            Close
                        </button>
                    </div>
                </div>
            )}
        </div>
    );
}
