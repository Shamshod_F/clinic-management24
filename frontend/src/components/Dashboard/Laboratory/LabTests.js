// ParentComponent.js
import React, { useEffect, useState } from "react";
import Requests from "./Elements/Requests";

export default function ParentComponent() {
  const [appointments, setAppointments] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    fetchAppointments();
  }, []);

  const fetchAppointments = async () => {
    try {
      const token = localStorage.getItem("accessToken");
      const response = await fetch("http://127.0.0.1:8000/api/laboratories", {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      if (!response.ok) {
        throw new Error("Failed to fetch appointments");
      }
      const data = await response.json();
      // Filter appointments with status of True
      const filteredAppointments = data.filter(appointment => appointment.status === true);
      setAppointments(filteredAppointments);
      setIsLoading(false);
    } catch (error) {
      console.error("Error fetching appointments:", error);
      setIsLoading(false);
    }
  };

  return (
    <div>
      <h2>Lab Tests</h2>
      {isLoading ? (
        <div>Loading...</div>
      ) : (
        <Requests appointments={appointments} />
      )}
    </div>
  );
}
