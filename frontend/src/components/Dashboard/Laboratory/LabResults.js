// ParentComponent.js
import React, { useEffect, useState } from "react";
import Results from "./Elements/Results";

export default function ParentComponent() {
  const [labResults, setLabResults] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    fetchLabResults();
  }, []);

  const fetchLabResults = async () => {
    try {
      const token = localStorage.getItem("accessToken");
      const response = await fetch("http://127.0.0.1:8000/api/lab-results", {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      if (!response.ok) {
        throw new Error("Failed to fetch lab results");
      }
      const data = await response.json();
      setLabResults(data);
      setIsLoading(false);
    } catch (error) {
      console.error("Error fetching lab results:", error);
      setIsLoading(false);
    }
  };

  return (
    <div>
      <h2>Lab Results</h2>
      {isLoading ? (
        <div>Loading...</div>
      ) : (
        <Results labResults={labResults} />
      )}
    </div>
  );
}
