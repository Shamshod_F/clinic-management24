import React from "react";

export default function NutritionData({ el }) {
  return (
    <tr>
      <td>{el.name}</td>
      <td>{el.serving_size_g}g</td>
      <td>{el.calories}</td>
      <td>{el.fat_total_g}g</td>
      <td>{el.fat_saturated_g}g</td>
      <td>{el.cholesterol_mg}mg</td>
      <td>{el.sodium_mg}mg</td>
      <td>{el.carbohydrates_total_g}g</td>
      <td>{el.fiber_g}g</td>
      <td>{el.sugar_g}g</td>
      <td>{el.protein_g}g</td>
    </tr>
  );
}
