import React, {useState} from "react";
import {ToastContainer, toast} from "react-toastify";

// setting minimum date for select date inputs
const TODAY = new Date().toISOString().split("T")[0];

export default function TakeAppointment() {
    const [name, setName] = useState("");
    const [surname, setSurname] = useState("");
    const [contact, setContact] = useState("");
    const [doctorType, setDoctorType] = useState("");
    const [date, setDate] = useState("");
    const [chosenHour, setChosenHour] = useState("");
    const [doctors, setDoctors] = useState([]);
    const [selectedDoctor, setSelectedDoctor] = useState(null);
    const [freeTime, setFreeTime] = useState([]);

    // notify user
    const notify = (message, type) =>
        toast[type](message, {
            position: "top-center",
            autoClose: 3000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "light",
        });
    const clearForm = () => {
        setName("");
        setSurname("");
        setContact("");
        setDoctorType("");
        setDate("");
        setChosenHour("");
        setSelectedDoctor(null);
        setFreeTime([]);
    };

    // handling form submission
    const handleSubmit = (e) => {
        e.preventDefault();

        // Retrieve the access token from wherever it's stored (e.g., localStorage)
        const accessToken = localStorage.getItem("accessToken");

        // Create the appointment object
        const appointmentData = {
            patient_name: name,
            patient_surname: surname,
            contact_info: contact,
            date: date,
            status: true, // Assuming status is always true
            doctor: selectedDoctor,
            start_time: chosenHour.split("-")[0], // Extracting start time
            end_time: chosenHour.split("-")[1], // Extracting end time
        };

        // Sending POST request with Bearer token
        fetch("http://127.0.0.1:8000/api/appointments/", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${accessToken}` // Include Bearer token in Authorization header
            },
            body: JSON.stringify(appointmentData),
        })
            .then((response) => {
                if (!response.ok) {
                    throw new Error("Failed to create appointment");
                }
                return response.json();
            })
            .then((data) => {
                // Handle success response
                console.log(data);
                notify("Appointment created successfully!", "success");
                // Clearing inputs after successful submission
                clearForm();
            })
            .catch((error) => {
                // Handle error
                console.error(error);
                notify("Failed to create appointment", "error");
            });
    };


    const handleDate = (e) => {
        setDate(e.target.value);
    };

    const handleDoctorType = (e) => {
        setDoctorType(e.target.value);
    };

    const handleGetDoctors = () => {
        if (doctorType !== "") {
                fetch(`http://127.0.0.1:8000/api/doctors-by-type/${doctorType}/`)
                .then((response) => {
                    if (!response.ok) {
                        throw new Error("Failed to fetch doctors");
                    }
                    return response.json();
                })
                .then((data) => {
                    setDoctors(data);
                    console.log(data); // Handle the response data as needed
                })
                .catch((error) => {
                    console.error(error);
                    notify("Failed to fetch doctors", "error");
                });
        } else {
            notify("Please select a doctor type", "error");
        }
    };

    const handleGetFreeTime = () => {
        if (selectedDoctor && date !== "") {
            fetch(
                `http://127.0.0.1:8000/api/get-free-time/${selectedDoctor}/${date}/`
            )
                .then((response) => {
                    if (!response.ok) {
                        throw new Error("Failed to fetch free time");
                    }
                    return response.json();
                })
                .then((data) => {
                    setFreeTime(data);
                    console.log(data); // Handle the response data as needed
                })
                .catch((error) => {
                    console.error(error);
                    notify("Failed to fetch free time", "error");
                });
        } else {
            notify("Please select a doctor and a date", "error");
        }
    };

    const handleDoctorSelect = (doctorId) => {
        setSelectedDoctor(doctorId);
    };

    return (
        <div className="d-flex flex-column">
            <ToastContainer
                position="top-center"
                autoClose={3000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="light"
            />

            <form
                onSubmit={handleSubmit}
                className="row d-flex flex-column gap-4 align-content-center "
            >
                <label className="col-12 col-lg-8 d-flex flex-column gap-2">
                    Name:
                    <input
                        value={name}
                        onChange={(e) => setName(e.target.value)}
                        className="form-control "
                        type="text"
                    />
                </label>
                <label className="col-12 col-lg-8  d-flex flex-column gap-2">
                    Surname:
                    <input
                        value={surname}
                        onChange={(e) => setSurname(e.target.value)}
                        className="form-control"
                        type="text"
                    />
                </label>
                <label className="col-12 col-lg-8  d-flex flex-column gap-2">
                    Contact info (phone/email):
                    <input
                        value={contact}
                        onChange={(e) => setContact(e.target.value)}
                        className="form-control "
                        type="text"
                    />
                </label>

                <label className="col-12 col-lg-8  d-flex flex-column gap-2">
                    Select Doctor Type:
                    <select
                        className="form-select"
                        value={doctorType}
                        onChange={handleDoctorType}
                    >
                        <option disabled value="">
                            Select...
                        </option>
                        <option value="cardiologist">Cardiologist</option>
                        <option value="dermatologist">Dermatologist</option>
                        <option value="gynecologist">Gynecologist</option>
                    </select>
                </label>

                <button
                    type="button"
                    className="btn btn-primary col-12 col-md-3 align-self-center mt-3"
                    onClick={handleGetDoctors}
                >
                    Get Doctors
                </button>

                {doctors.length > 0 && (
                    <div className="col-12 col-lg-8">
                        <h5>Select Doctor:</h5>
                        <ul className="list-group">
                            {doctors.map((doctor) => (
                                <li
                                    key={doctor.id}
                                    className={`list-group-item ${
                                        selectedDoctor === doctor.id ? "active" : ""
                                    }`}
                                    onClick={() => handleDoctorSelect(doctor.id)}
                                    style={{cursor: "pointer"}}
                                >
                                    {doctor.first_name} {doctor.last_name}
                                </li>
                            ))}
                        </ul>
                    </div>
                )}

                <label className="col-12 col-lg-8">
                    Select Date:
                    <input
                        min={TODAY}
                        type="date"
                        className="form-control"
                        value={date}
                        onChange={handleDate}
                    />
                </label>

                <button
                    type="button"
                    className="btn btn-primary co l-12 col-md-3 align-self-center mt-3"
                    onClick={handleGetFreeTime}
                >
                    Show Free Time
                </button>

                {freeTime.length > 0 && (
                    <div className="col-12 col-lg-8">
                        <h5>Select available time:</h5>
                        <ul className="list-group">
                            {freeTime.map((timeSlot) => (
                                <li
                                    key={`${timeSlot.start_time}-${timeSlot.end_time}`}
                                    className={`list-group-item ${
                                        chosenHour === `${timeSlot.start_time}-${timeSlot.end_time}` ? "active" : ""
                                    }`}
                                    onClick={() => setChosenHour(`${timeSlot.start_time}-${timeSlot.end_time}`)}
                                    style={{cursor: "pointer"}}
                                >
                                    {`${timeSlot.start_time} - ${timeSlot.end_time}`}
                                </li>
                            ))}
                        </ul>
                    </div>
                )}


                <button
                    type="submit"
                    className="btn btn-primary col-12 col-md-3 align-self-center mt-5"
                >
                    Submit
                </button>
            </form>
        </div>
    );
}
