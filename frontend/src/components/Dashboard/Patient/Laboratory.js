import React, {useState, useEffect} from "react";
import {ToastContainer, toast} from "react-toastify";
import {ImLab} from "react-icons/im";
import {TiArrowLeft} from "react-icons/ti";
import "react-toastify/dist/ReactToastify.css";

const TODAY = new Date().toISOString().split("T")[0];

export default function Laboratory() {
    const [name, setName] = useState("");
    const [contact, setContact] = useState("");
    const [testName, setTestName] = useState("");
    const [date, setDate] = useState("");
    let [time, setTime] = useState("");
    const [toggle, setToggle] = useState(false);
    const [results, setResults] = useState([]);

    const handleToggle = () => {
        setToggle((prev) => !prev);
    };

    const notify = (message, type) =>
        toast[type](message, {
            position: "top-center",
            autoClose: 3000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "light",
        });

    const handleSubmit = async (e) => {
        e.preventDefault();
        const access = localStorage.getItem('accessToken');
        try {
            const response = await fetch("http://127.0.0.1:8000/api/laboratories/", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${access}`,
                },
                body: JSON.stringify({
                    patient_name: name,
                    contact_info: contact,
                    test_type: testName,
                    date: date,
                    time: time,
                }),
            });
            if (!response.ok) {
                throw new Error("Failed to submit laboratory request");
            }
            notify("You took an appointment for lab tests successfully!", "success");

            setName("");
            setContact("");
            setTestName("");
            setDate("");
            setTime("");
        } catch (error) {
            console.error("Error submitting laboratory request:", error);
            notify("Failed to submit laboratory request", "error");
        }
    };

    const handleDate = (e) => {
        setDate(e.target.value);
    };

    useEffect(() => {
        const fetchResults = async () => {
            try {
                const access = localStorage.getItem('accessToken');
                const response = await fetch(`http://127.0.0.1:8000/api/user-lab-result/`, {
                    method: "GET",
                    headers: {
                        Authorization: `Bearer ${access}`,
                    },
                });
                if (!response.ok) {
                    throw new Error("Failed to fetch laboratory results");
                }
                const data = await response.json();
                setResults(data);
            } catch (error) {
                console.error("Error fetching laboratory results:", error);
            }
        };
        fetchResults();
    }, []);

    return (
        <div>
            {toggle ? (
                <div>
                    <div className="">
                        <button onClick={handleToggle} className="btn bg-secondary-subtle ">
                            <TiArrowLeft/> Back
                        </button>
                    </div>
                    <div className="d-flex flex-column pt-4 gap-2">
                        <div className="col-12 d-flex justify-content-around pt-2 pb-2 border-top  border-bottom ">
                            <span>Test Result</span>
                            <span>Date</span>
                            <span>Time</span>
                        </div>
                        {results.map((result, index) => (
                            <div
                                key={index}
                                className={`d-flex flex-column flex-sm-row align-items-center justify-content-sm-around gap-3 gap-sm-0 p-2 ${result.status === "positive" ? "bg-success-subtle" : "bg-danger-subtle"} rounded-2`}
                            >
                                <span>{result.status}</span>
                                <span>On: {result.date}</span>
                                {result.time && (
                                    <span>At: {result.time.split(":")[0]}:{result.time.split(":")[1]}</span>
                                )}
                            </div>
                        ))}
                    </div>
                </div>
            ) : (
                <>
                    <div className="d-flex justify-content-end ">
                        <button className="btn btn-warning" onClick={handleToggle}>
                            Results
                        </button>
                    </div>
                    <div className="d-flex flex-column ">
                        <ToastContainer
                            position="top-center"
                            autoClose={3000}
                            hideProgressBar={false}
                            newestOnTop={false}
                            closeOnClick
                            rtl={false}
                            pauseOnFocusLoss
                            draggable
                            pauseOnHover
                            theme="light"
                        />
                        <div className="d-flex justify-content-center pb-2 ">
                            <ImLab size={45}/>
                        </div>

                        <form
                            onSubmit={handleSubmit}
                            className="row d-flex flex-column gap-4 align-content-center "
                        >
                            <label className="col-12 col-lg-8 d-flex flex-column gap-2 ">
                                Name:
                                <input
                                    value={name}
                                    onChange={(e) => setName(e.target.value)}
                                    className="form-control "
                                    type="text"
                                />
                            </label>
                            <label className="col-12 col-lg-8  d-flex flex-column gap-2">
                                Contact info (phone/email):
                                <input
                                    value={contact}
                                    onChange={(e) => setContact(e.target.value)}
                                    className="form-control "
                                    type="text"
                                />
                            </label>

                            <label className="col-12 col-lg-8  d-flex flex-column gap-2">
                                Select Test:
                                <select
                                    className="form-select"
                                    value={testName}
                                    onChange={(e) => {
                                        setTestName(e.target.value);
                                    }}
                                >
                                    <option disabled value="">
                                        Select...
                                    </option>
                                    <option value="basic-metabolic">Basic Metobolic Panel</option>
                                    <option value="comprehensive-metabolic">
                                        Comprehensive Metabolic Panel
                                    </option>
                                    <option value="lipid-profile">Lipid profile</option>
                                    <option value="pregnancy-test">Pregnancy test</option>
                                    <option value="urinalysis"> Urinalysis</option>
                                </select>
                            </label>

                            <label className="col-12 col-lg-8 ">
                                Select Date:
                                <input
                                    min={TODAY}
                                    type="date"
                                    className="form-control"
                                    value={date}
                                    onChange={handleDate}
                                />
                            </label>

                            <label className="col-12 col-lg-8 ">
                                Select Time:
                                <input
                                    type="time"
                                    className="form-control"
                                    value={time}
                                    onChange={(e) => setTime(e.target.value)}
                                />
                            </label>

                            <button
                                type="submit"
                                className="btn btn-primary col-12 col-md-3 align-self-center mt-5"
                            >
                                Submit
                            </button>
                        </form>
                    </div>
                </>
            )}
        </div>
    );
}
