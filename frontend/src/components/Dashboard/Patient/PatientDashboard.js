import React, { useEffect, useState } from "react";

import Header from "../../Layout/Header";
import Loader from "../../../common/Loader";
import Footer from "../../Layout/Footer";

import HomePage from "./HomePage";
import MyAppointments from "./MyAppointments";
import TakeAppointment from "./TakeAppointment";
import Nutrition from "./Nutrition";
import DoctorsResponse from "./DoctorsResponse";
// import OnlineChat from "./OnlineChat";
import Nurse from "./Nurse";
import Laboratory from "./Laboratory";

export default function PatientDashboard() {
  const [isLoading, setIsLoading] = useState(true);
  const [activeButton, setActiveButton] = useState("Home Page");

  // button clicks
  const handleButtonClick = (buttonName) => {
    setActiveButton(buttonName);
  };

  // loader
  useEffect(() => {
    setTimeout(() => setIsLoading(false), 1800);
  }, []);
  if (isLoading) {
    return <Loader />;
  }

  return (
    <div className="dashboard">
      <Header />
      <div className="d-flex justify-content-around flex-column  flex-md-row gap-2 me-3 ms-3 me-md-0 ms-md-0">
        <aside
          style={{ backgroundColor: "#ffff", height: "min-content" }}
          className="col-12 col-md-3  p-0 rounded-2"
        >
          {/* /start/ menu for mobile app */}
          <select
            className="menu__mobile form-select p-3"
            value={activeButton}
            onChange={(e) => setActiveButton(e.target.value)}
          >
            <option value="Home Page">Home Page</option>
            <option value="Take An Appointment">Take An Appointment</option>
            <option value="My Appointments">My Appointments</option>
            <option value="Nutrition">Nutrition</option>
            <option value="Doctor's Response">Doctor's Response</option>
            {/*<option value="Online Chat">Online Chat</option>*/}
            <option value="Nurse">Nurse</option>
            <option value="Laboratory">Laboratory</option>
          </select>
          {/* /end/ menu for mobile app */}
          <ul className="list-unstyled row  m-2 mb-0 menu__desktop ">
            <li
              className={`p-4 mt-1 ${
                activeButton === "Home Page" ? "active" : ""
              }`}
              onClick={() => handleButtonClick("Home Page")}
            >
              <h5>Home Page</h5>
            </li>
            <li
              className={`p-4 mt-2 ${
                activeButton === "Take An Appointment" ? "active" : ""
              }`}
              onClick={() => handleButtonClick("Take An Appointment")}
            >
              <h5>Take An Appointment</h5>
            </li>
            <li
              className={`p-4 mt-2 ${
                activeButton === "My Appointments" ? "active" : ""
              }`}
              onClick={() => handleButtonClick("My Appointments")}
            >
              <h5>My Appointments</h5>
            </li>
            <li
              className={`p-4 mt-2 ${
                activeButton === "Nutrition" ? "active" : ""
              }`}
              onClick={() => handleButtonClick("Nutrition")}
            >
              <h5>Nutrition</h5>
            </li>
            <li
              className={`p-4 mt-2 ${
                activeButton === "Doctor's Response" ? "active" : ""
              }`}
              onClick={() => handleButtonClick("Doctor's Response")}
            >
              <h5>Doctor's Response</h5>
            </li>
            {/*<li*/}
            {/*  className={`p-4 mt-2 ${*/}
            {/*    activeButton === "Online Chat" ? "active" : ""*/}
            {/*  }`}*/}
            {/*  onClick={() => handleButtonClick("Online Chat")}*/}
            {/*>*/}
            {/*  <h5>Online Chat</h5>*/}
            {/*</li>*/}
            <li
              className={`p-4 mt-2 ${activeButton === "Nurse" ? "active" : ""}`}
              onClick={() => handleButtonClick("Nurse")}
            >
              <h5>Nurse</h5>
            </li>
            <li
              className={`p-4 mt-2 ${
                activeButton === "Laboratory" ? "active" : ""
              }`}
              onClick={() => handleButtonClick("Laboratory")}
            >
              <h5>Laboratory</h5>
            </li>
          </ul>
        </aside>
        <main
          className="col-12 col-md-8 rounded-2 p-3 p-sm-5"
          style={{ backgroundColor: "#ffff", height: "fit-content" }}
        >
          {activeButton === "Home Page" && <HomePage />}
          {activeButton === "Take An Appointment" && <TakeAppointment />}
          {activeButton === "My Appointments" && <MyAppointments />}
          {activeButton === "Nutrition" && <Nutrition />}
          {activeButton === "Doctor's Response" && <DoctorsResponse />}
          {/*{activeButton === "Online Chat" && <OnlineChat />}*/}
          {activeButton === "Nurse" && <Nurse />}
          {activeButton === "Laboratory" && <Laboratory />}
        </main>
      </div>
      <Footer />
    </div>
  );
}
