import React, { useState, useEffect } from "react";
import { MdDelete } from "react-icons/md";
import { toast } from "react-toastify";

export default function Upcoming() {
    const [appointments, setAppointments] = useState([]);

    const handleDelete = async (id) => {
        try {
            const accessToken = localStorage.getItem("accessToken");
            const response = await fetch(`http://127.0.0.1:8000/api/appointments/${id}/`, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${accessToken}`
                },
            });

            if (!response.ok) {
                throw new Error('Failed to delete appointment');
            }

            // Remove the deleted appointment from the state
            setAppointments((prevAppointments) => prevAppointments.filter(appointment => appointment.id !== id));

            toast.success("Appointment deleted successfully!");
        } catch (error) {
            console.error(error);
            toast.error("Failed to delete appointment. Please try again later.");
        }
    };

    useEffect(() => {
        const fetchData = async () => {
            try {
                const username = localStorage.getItem("username");
                const accessToken = localStorage.getItem("accessToken");
                const [response] = await Promise.all([fetch(`http://127.0.0.1:8000/api/appointments/active/${username}/`, {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${accessToken}`
                    },
                })]);

                if (!response.ok) {
                    throw new Error('Failed to fetch data');
                }

                const data = await response.json();

                // Fetch doctor's details for each appointment
                const updatedAppointments = await Promise.all(data.map(async (appointment) => {
                    const doctorResponse = await fetch(`http://127.0.0.1:8000/api/doctors/${appointment.doctor}/`, {
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': `Bearer ${accessToken}`
                        },
                    });

                    if (!doctorResponse.ok) {
                        throw new Error('Failed to fetch doctor data');
                    }

                    const doctorData = await doctorResponse.json();
                    return {...appointment, doctor: `${doctorData.first_name} ${doctorData.last_name}`};
                }));

                setAppointments(updatedAppointments);
            } catch (error) {
                console.error(error);
            }
        };

        fetchData();
    }, []);

    return (
        <div className="d-flex flex-column pt-4 gap-2">
            {appointments.map((appointment) => (
                <div key={appointment.id}
                     className="d-flex flex-column flex-sm-row align-items-center justify-content-sm-around gap-3 gap-sm-0 p-2 bg-success-subtle rounded-2">
                    <span>To: {appointment.doctor}</span>
                    <span>On: {appointment.date}</span>
                    <span>At: {appointment.start_time} - {appointment.end_time}</span>
                    <div className="d-flex gap-4">
                        <MdDelete size={20} onClick={() => handleDelete(appointment.id)} />
                    </div>
                </div>
            ))}
        </div>
    );
}
