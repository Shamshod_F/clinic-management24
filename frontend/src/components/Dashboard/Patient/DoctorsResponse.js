import React, { useEffect, useState } from "react";

export default function DoctorsResponse() {
  const [doctorResponses, setDoctorResponses] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    const fetchDoctorResponses = async () => {
      try {
        const storedUsername = localStorage.getItem("username");
        if (!storedUsername) {
          throw new Error("Username not found in localStorage");
        }

        const response = await fetch(`http://127.0.0.1:8000/api/doctor-response/patient/${storedUsername}/`);
        if (!response.ok) {
          throw new Error("Failed to fetch doctor responses");
        }

        const data = await response.json();
        setDoctorResponses(data);
        setLoading(false);
      } catch (error) {
        setError(error.message);
        setLoading(false);
      }
    };

    fetchDoctorResponses();
  }, []);

  if (loading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error: {error}</div>;
  }

  return (
    <div className="table-responsive">
      <table className="table table-bordered">
        <thead>
          <tr>
            <th>Doctor</th>
            <th>Date</th>
            <th>Time</th>
            <th>Room</th>
          </tr>
        </thead>
        <tbody>
          {doctorResponses.map((response, index) => (
            <tr key={index} className="bg-light-green">
              <td className="bg-light-green">{response.appointment.patient_name} {response.appointment.patient_surname}</td>
              <td>{response.date}</td>
              <td>{formatTime(response.appointment.start_time)} - {formatTime(response.appointment.end_time)}</td>
              <td>{response.room}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

function formatTime(timeString) {
  const [hours, minutes] = timeString.split(":");
  return `${hours}:${minutes}`;
}
