import React, { useState } from "react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const TODAY = new Date().toISOString().split("T")[0];

export default function Nurse() {
  const [patientName, setPatientName] = useState("");
  const [contactInfo, setContactInfo] = useState("");
  const [date, setDate] = useState("");
  const [time, setTime] = useState("");

  const notify = (message, type) =>
    toast[type](message, {
      position: "top-center",
      autoClose: 3000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      theme: "light",
    });

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (
      patientName.trim() === "" ||
      contactInfo.trim() === "" ||
      date.trim() === "" ||
      time.trim() === ""
    ) {
      notify("One of the inputs is empty or select a date/hour", "error");
    } else {
      try {
        const accessToken = localStorage.getItem("accessToken");

        const response = await fetch("http://127.0.0.1:8000/api/nurse-book/", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${accessToken}`,
          },
          body: JSON.stringify({
            patient_name: patientName,
            contact_info: contactInfo,
            date: date,
            time: time,
          }),
        });

        if (response.ok) {
          notify(
            "You took an appointment successfully!. Wait nurses' call",
            "success"
          );
          setPatientName("");
          setContactInfo("");
          setDate("");
          setTime("");
        } else {
          notify(
            "Failed to take appointment. Please try again later.",
            "error"
          );
        }
      } catch (error) {
        console.error("Error:", error);
        notify("Something went wrong!", "error");
      }
    }
  };

  const handleDate = (e) => {
    setDate(e.target.value);
  };

  return (
    <div className="d-flex flex-column ">
      <ToastContainer
        position="top-center"
        autoClose={3000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="light"
      />

      <form
        onSubmit={handleSubmit}
        className="row d-flex flex-column gap-4 align-content-center "
      >
        <label className="col-12 col-lg-8 d-flex flex-column gap-2 ">
          Patient Name:
          <input
            value={patientName}
            onChange={(e) => setPatientName(e.target.value)}
            className="form-control "
            type="text"
          />
        </label>
        <label className="col-12 col-lg-8  d-flex flex-column gap-2">
          Contact info (phone):
          <input
            value={contactInfo}
            onChange={(e) => setContactInfo(e.target.value)}
            className="form-control "
            type="text"
          />
        </label>

        <label className="col-12 col-lg-8 ">
          Select Date:
          <input
            min={TODAY}
            type="date"
            className="form-control"
            value={date}
            onChange={handleDate}
          />
        </label>

        <label className="col-12  col-lg-4 ">
          Select Time:
          <input
            type="time"
            className="form-control"
            value={time}
            onChange={(e) => setTime(e.target.value)}
          />
        </label>

        <button
          type="submit"
          className="btn btn-primary col-12 col-md-3 align-self-center mt-5"
        >
          Submit
        </button>
      </form>
    </div>
  );
}
