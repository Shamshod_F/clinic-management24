import React, { useState } from "react";
import axios from "axios";
import NutritionData from "./NutritionData";

export default function Nutrition() {
  const [inputData, setInputData] = useState("");
  const [data, setData] = useState([]);
  let totalServing = 0;
  let totalCalories = 0;
  let totalFat = 0;
  let totalSaturated = 0;
  let totalCholesterol = 0;
  let totalSodium = 0;
  let totalCarbohydrates = 0;
  let totalFiber = 0;
  let totalSugar = 0;
  let totalProtein = 0;

  if (data) {
    data.forEach((e) => {
      totalServing += e.serving_size_g;
      totalCalories += e.calories;
      totalFat += e.fat_total_g;
      totalSaturated += e.fat_saturated_g;
      totalCholesterol += e.cholesterol_mg;
      totalSodium += e.sodium_mg;
      totalCarbohydrates += e.carbohydrates_total_g;
      totalFiber += e.fiber_g;
      totalSugar += e.sugar_g;
      totalProtein += e.protein_g;
    });
  }
  const handleClick = () => {
    axios
      .get(`https://api.calorieninjas.com/v1/nutrition?query=${inputData}`, {
        headers: {
          "X-Api-Key": "ObuOpY3tMGrfkkNuYt1rxA==GvJgmCgaXRg2cVw4",
        },
      })
      .then((response) => {
        setData(response.data.items);
      })
      .catch((error) => {
        console.error(error);
      });
  };
  return (
    <div className="d-flex flex-column gap-4 ">
      <input
        value={inputData}
        onChange={(e) => setInputData(e.target.value)}
        placeholder="Enter some text to calculate nutrition information."
        type="text"
        className="form-control"
      />
      <div className="pt-4 " style={{ overflow: "scroll", maxHeight: "500px" }}>
        <div style={{ minWidth: "700px" }}>
          <h2 className="text-center">Nutrition Results</h2>
          <table className="table text-center  table-striped ">
            <thead className="table-dark">
              <tr>
                <th>Name</th>
                <th>Serving Size</th>
                <th>Calories</th>
                <th>Total Fat</th>
                <th>Saturated Fat</th>
                <th>Cholesterol</th>
                <th>Sodium</th>
                <th>Carbohydrates</th>
                <th>Fiber</th>
                <th>Sugar</th>
                <th>Protein</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th>Total</th>
                <th>{totalServing.toFixed(1)}g</th>
                <th>{totalCalories.toFixed(1)}</th>
                <th>{totalFat.toFixed(1)}g</th>
                <th>{totalSaturated.toFixed(1)}g</th>
                <th>{totalCholesterol.toFixed(1)}mg</th>
                <th>{totalSodium.toFixed(1)}mg</th>
                <th>{totalCarbohydrates.toFixed(1)}g</th>
                <th>{totalFiber.toFixed(1)}g</th>
                <th>{totalSugar.toFixed(1)}g</th>
                <th>{totalProtein.toFixed(1)}g</th>
              </tr>
              {data.map((el, index) => (
                <NutritionData key={index} el={el} />
              ))}
            </tbody>
          </table>
        </div>
      </div>
      <div className="d-flex justify-content-center pt-4">
        <button onClick={handleClick} className="btn btn-primary">
          Get Nutrition
        </button>
      </div>
    </div>
  );
}
