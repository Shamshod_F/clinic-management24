import React, { useEffect, useState } from "react";

const TODAY = new Date().toISOString().split("T")[0];

const Appointments = () => {
    const [username, setUsername] = useState("");
    const [selectedDate, setSelectedDate] = useState(TODAY);
    const [appointments, setAppointments] = useState([]);
    const [selectedAppointment, setSelectedAppointment] = useState(null);
    const [roomNumber, setRoomNumber] = useState("");
    const [notification, setNotification] = useState(null);

    const fetchAppointments = async (username) => {
        try {
            username = localStorage.getItem("username");
            const response = await fetch(`http://127.0.0.1:8000/api/appointments/doctor/${username}`);
            if (!response.ok) {
                throw new Error("Failed to fetch appointments");
            }
            const data = await response.json();
            setAppointments(data);
        } catch (error) {
            console.error(error);
        }
    };

    useEffect(() => {
        const storedUsername = localStorage.getItem("username");
        if (storedUsername) {
            setUsername(storedUsername);
            fetchAppointments(storedUsername);
        }
    }, []);

    useEffect(() => {
        fetchAppointments(username);
    }, [username]);

    const handleDateChange = (e) => {
        const selected = e.target.value;
        setSelectedDate(selected || TODAY);
    };

    const handleAcceptAppointment = (appointment) => {
        // Check if a response already exists for this appointment
        if (appointment.response_exists) {
            setNotification("Doctor response already exists for this appointment.");
            return;
        }
        setSelectedAppointment(appointment);
    };

    const handleRoomNumberChange = (e) => {
        setRoomNumber(e.target.value);
    };

    const handleAcceptAppointmentSubmit = async () => {
        if (!roomNumber) {
            setNotification("Room number is required.");
            return;
        }

        try {
            const response = await fetch(`http://127.0.0.1:8000/api/doctor-response/`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    appointment: selectedAppointment.id,
                    room: roomNumber,
                    date: selectedDate,
                }),
            });
            if (!response.ok) {
                throw new Error('Failed to accept appointment');
            }
            const data = await response.json();
            console.log('Appointment accepted successfully:', data);
            updateAppointmentStatus(selectedAppointment.id, false); // Update status to false
            setSelectedAppointment(null);
            setRoomNumber("");
        } catch (error) {
            console.error('Error accepting appointment:', error);
        }
    };

    const updateAppointmentStatus = async (appointmentId, status) => {
        try {
            const response = await fetch(`http://127.0.0.1:8000/api/appointments/${appointmentId}/update-status/`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    status: status,
                }),
            });
            if (!response.ok) {
                throw new Error('Failed to update appointment status');
            }
            const data = await response.json();
            console.log('Appointment status updated successfully:', data);
            // Refresh appointments after status update
            fetchAppointments(username);
        } catch (error) {
            console.error('Error updating appointment status:', error);
        }
    };

    const renderAppointments = (appointments) => (
        appointments.map((appointment, index) => (
            <tr key={index}>
                <td>{appointment.patient_name} {appointment.patient_surname}</td>
                <td>{appointment.contact_info}</td>
                <td>{appointment.date}</td>
                <td>{appointment.start_time} - {appointment.end_time}</td>
                <td>
                    {appointment.status ? (
                        <button className="btn btn-primary" onClick={() => handleAcceptAppointment(appointment)}>Accept</button>
                    ) : null}
                </td>
            </tr>
        ))
    );

    return (
        <div>
            {notification && (
                <div className="alert alert-danger" role="alert">
                    {notification}
                </div>
            )}

            <div className="mb-3">
                <label className="form-label">
                    Select date:
                    <input type="date" className="form-control" value={selectedDate} min={TODAY}
                           onChange={handleDateChange} />
                </label>
            </div>

            <div className="table-responsive">
                <table className="table table-bordered">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Phone Number</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {renderAppointments(appointments)}
                    </tbody>
                </table>
            </div>

            {selectedAppointment && (
                <div className="modal" style={{ display: "block", backgroundColor: "rgba(0, 0, 0, 0.5)" }}>
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title">Enter Room Number</h5>
                                <button type="button" className="btn-close" aria-label="Close"
                                    onClick={() => setSelectedAppointment(null)}></button>
                            </div>
                            <div className="modal-body">
                                <div className="mb-3">
                                    <label htmlFor="roomNumber" className="form-label">Room Number:</label>
                                    <input type="text" className="form-control" id="roomNumber" value={roomNumber}
                                        onChange={handleRoomNumberChange} />
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary"
                                    onClick={() => setSelectedAppointment(null)}>Close
                                </button>
                                <button type="button" className="btn btn-primary"
                                    onClick={handleAcceptAppointmentSubmit}>Submit
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            )}
        </div>
    );
};

export default Appointments;
