import React, { useState } from "react";

const EditStatusModal = ({ requestId, onUpdateStatus, onClose }) => {
  const [newStatus, setNewStatus] = useState("");

  const handleStatusChange = (e) => {
    setNewStatus(e.target.value);
  };

  const handleSubmit = () => {
    onUpdateStatus(requestId, newStatus);
    onClose(); // Close the modal after updating status
  };

  return (
    <div className="modal">
      <h2>Edit Status</h2>
      <select value={newStatus} onChange={handleStatusChange}>
        <option value="status1">Status 1</option>
        <option value="status2">Status 2</option>
        {/* Add more options as needed */}
      </select>
      <button onClick={handleSubmit}>Update Status</button>
    </div>
  );
};

export default EditStatusModal;
