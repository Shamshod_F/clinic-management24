import React, { useState, useEffect } from "react";
import "./Requests.css"; // Import custom CSS file for styling

const Accepted = ({ filteredDate }) => {
  const [requests, setRequests] = useState([]);
  useEffect(() => {
    const fetchAccepted = async () => {
      try {
        const response = await fetch("http://127.0.0.1:8000/api/nurse-book-list/accepted");
        if (!response.ok) {
          throw new Error("Failed to fetch Inactives");
        }
        const data = await response.json();
        setRequests(data);
      } catch (error) {
        console.error("Error fetching Inactives:", error);
      }
    };

    fetchAccepted();
  }, []);


  const formatTime = (timeString) => {
    if (timeString && timeString.length >= 5) {
      return timeString.slice(0, 5);
    } else {
      return "Invalid Time";
    }
  };

  const filteredAccepted = filteredDate
    ? requests.filter(request => request.date === filteredDate)
    : requests;

  return (
    <div>
      <table className="requests-table">
        <thead>
          <tr>
            <th>Patient</th>
            <th>Phone Number</th>
            <th>Date</th>
            <th>Time</th>
          </tr>
        </thead>
        <tbody>
          {filteredAccepted.map((request) => (
            <tr key={request.id}>
              <td>{request.patient_name}</td>
              <td>{request.contact_info}</td>
              <td>{request.date}</td>
              <td>{formatTime(request.time)}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default Accepted;
