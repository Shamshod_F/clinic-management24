import React, { useState, useEffect } from "react";
import "./Requests.css";

const Requests = ({ filteredDate }) => {
  const [requests, setRequests] = useState([]);

  const fetchRequests = async () => {
    try {
      const response = await fetch("http://127.0.0.1:8000/api/nurse-book-list/request");
      if (!response.ok) {
        throw new Error("Failed to fetch requests");
      }
      const data = await response.json();
      setRequests(data);
    } catch (error) {
      console.error("Error fetching requests:", error);
    }
  };

  useEffect(() => {
    fetchRequests();
  }, []);

  const formatTime = (timeString) => {
    if (timeString && timeString.length >= 5) {
      return timeString.slice(0, 5);
    } else {
      return "Invalid Time";
    }
  };

 const handleStatusUpdate = async (id, newStatus) => {
  try {
    const accessToken = localStorage.getItem("accessToken");
    const response = await fetch(`http://127.0.0.1:8000/api/nurse-book/update/${id}/`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${accessToken}`,
      },
      body: JSON.stringify({ status: newStatus })
    });
    if (!response.ok) {
      throw new Error("Failed to update status");
    }
    // Assuming the request is successful, update the local state or fetch requests again to reflect changes
    await fetchRequests();
  } catch (error) {
    console.error("Error updating status:", error);
  }
};


  const filteredRequests = filteredDate
    ? requests.filter(request => request.date === filteredDate)
    : requests;

  return (
    <div>
      <table className="requests-table">
        <thead>
          <tr>
            <th>Patient</th>
            <th>Phone Number</th>
            <th>Date</th>
            <th>Time</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {filteredRequests.map((request) => (
            <tr key={request.id}>
              <td>{request.patient_name}</td>
              <td>{request.contact_info}</td>
              <td>{request.date}</td>
              <td>{formatTime(request.time)}</td>
              <td>
                <button className="button-accept" onClick={() => handleStatusUpdate(request.id, "accepted")}>
                  Accept
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default Requests;
