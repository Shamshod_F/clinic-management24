
import React, { useState } from "react";
import Requests from "./Elements/Requests";
import Accepted from "./Elements/Accepted";
import "./OrderList.css";

const TODAY = new Date().toISOString().split("T")[0];

const OrderList = () => {
  const [filteredDate, setFilteredDate] = useState("");
  const [selectedType, setSelectedType] = useState("Requests");

  const handleDateChange = (e) => {
    setFilteredDate(e.target.value);
  };

  const handleTypeChange = (e) => {
    setSelectedType(e.target.value);
  };

  const clearFilter = () => {
    setFilteredDate("");
  };

  return (
    <div className="order-list-container">
      <header>
        <h1>Order List</h1>
      </header>
      <div className="filter-section">
        <label className="filter-label">
          Filter by date:
          <input
            type="date"
            className="filter-input"
            value={filteredDate}
            onChange={handleDateChange}
            min={TODAY}
          />
        </label>
        <button className="clear-button" onClick={clearFilter}>
          Clear
        </button>
        <div className="select-container">
          <select className="select" value={selectedType} onChange={handleTypeChange}>
            <option value="Requests">Requests</option>
            <option value="Accepted">Accepted</option>
          </select>
          <div className="select-arrow"></div>
        </div>
      </div>

      <div className="requests-container">
        {selectedType === "Requests" && <Requests filteredDate={filteredDate} />}
        {selectedType === "Accepted" && <Accepted filteredDate={filteredDate} />}
      </div>
    </div>
  );
};

export default OrderList;
