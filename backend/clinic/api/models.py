from django.contrib.admin import ModelAdmin
from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.hashers import make_password


class CustomUserManager(BaseUserManager):
    def create_user(self, username, password=None, **extra_fields):
        """
        Create and return a regular user with the given username and password.
        """
        if not username:
            raise ValueError(_('The Username field must be set'))
        user = self.model(username=username, **extra_fields)
        # password = make_password(password)
        print(password)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def edit_user(self, user, password=None, **extra_fields):
        """
        Edit the given user with the provided password and extra fields.
        """
        print("Editing user: ", user.username)  # Add this line
        if password:
            user.set_password(password)  # Use set_password to encrypt the password
            print("Hashed password: ", user.password)  # Add this line
        if extra_fields:
            for field, value in extra_fields.items():
                setattr(user, field, value)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, password=None, **extra_fields):
        """
        Create and return a superuser with the given username and password.
        """
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError(_('Superuser must have is_staff=True.'))
        if extra_fields.get('is_superuser') is not True:
            raise ValueError(_('Superuser must have is_superuser=True.'))

        return self.create_user(username, password, **extra_fields)

    def create_patient(self, username, password=None, **extra_fields):
        """
        Create and return a patient user with the given username and password.
        """
        extra_fields.setdefault('role', CustomUser.PATIENT)
        print(password)
        return self.create_user(username, password, **extra_fields)


class UserAdmin(ModelAdmin):
    def save_model(self, request, obj, form, change):
        if 'password' in form.changed_data:
            obj.set_password(obj.password)
        super().save_model(request, obj, form, change)


class CustomUser(AbstractUser):
    ADMIN = 'admin'
    DOCTOR = 'doctor'
    PATIENT = 'patient'
    LAB_TECH = 'lab_tech'
    NURSE = 'nurse'

    ROLE_CHOICES = [
        (ADMIN, 'Admin'),
        (DOCTOR, 'Doctor'),
        (PATIENT, 'Patient'),
        (LAB_TECH, 'Lab Technician'),
        (NURSE, 'Nurse'),
    ]

    role = models.CharField(max_length=20, choices=ROLE_CHOICES)

    objects = CustomUserManager()

    def save(self, *args, **kwargs):
        if not self.pk and not self.password.startswith(('pbkdf2_sha256$', 'bcrypt$', 'argon2')):
            self.password = make_password(self.password)
        super().save(*args, **kwargs)


class Doctor(models.Model):
    DOCTOR_TYPE_CHOICES = [
        ('cardiologist', 'Cardiologist'),
        ('dermatologist', 'Dermatologist'),
        ('gynecologist', 'Gynecologist'),
    ]
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE, limit_choices_to={'role': CustomUser.DOCTOR})
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    doctor_type = models.CharField(max_length=20, choices=DOCTOR_TYPE_CHOICES)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.first_name} {self.last_name}'


class Appointment(models.Model):
    patient = models.ForeignKey(CustomUser, on_delete=models.CASCADE, related_name='appointments',
                                limit_choices_to={'role': CustomUser.PATIENT})
    patient_name = models.CharField(max_length=50)
    patient_surname = models.CharField(max_length=50)
    contact_info = models.CharField(max_length=255)
    status = models.BooleanField(default=False)
    doctor = models.ForeignKey(Doctor, on_delete=models.CASCADE)
    date = models.DateField()
    start_time = models.TimeField()
    end_time = models.TimeField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def get_full_name(self):
        return f'{self.patient_name} {self.patient_surname}'

    def __str__(self):
        return self.get_full_name()


class DoctorResponse(models.Model):
    appointment = models.OneToOneField(Appointment, on_delete=models.CASCADE)
    room = models.CharField(max_length=50)
    date = models.DateField()

    def __str__(self):
        return f'{self.date} {self.room}'


class Nurse(models.Model):
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE, limit_choices_to={'role': CustomUser.NURSE})
    nurse_name = models.CharField(max_length=50)
    nurse_number = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.nurse_name} {self.nurse_number}'


class NurseBook(models.Model):
    STATUS_CHOICE = [
        ('request', 'Active'),
        ('accepted', 'Accepted'),
        ('waiting', 'Waiting'),
        ('inactive', 'Inactive'),
    ]

    patient = models.ForeignKey(CustomUser, on_delete=models.CASCADE, related_name='nurse_appointments',
                                limit_choices_to={'role': CustomUser.PATIENT})
    patient_name = models.CharField(max_length=50)
    contact_info = models.CharField(max_length=255)
    status = models.CharField(max_length=50, choices=STATUS_CHOICE, default='request')
    date = models.DateField()
    time = models.TimeField()

    def __str__(self):
        return f"{self.patient_name} {self.contact_info}"


class Laboratory(models.Model):
    TEST_TYPE_CHOICES = [
        ('basic-metabolic', 'Basic Metabolic Panel'),
        ('comprehensive-metabolic', 'Comprehensive Metabolic Panel'),
        ('lipid-profile', 'Lipid profile'),
        ('pregnancy-test', 'Pregnancy test'),
        ('urinalysis', 'Urinalysis'),
    ]

    patient = models.ForeignKey(CustomUser, on_delete=models.CASCADE, related_name='lab_appointments',
                                limit_choices_to={'role': CustomUser.PATIENT})
    patient_name = models.CharField(max_length=50)
    contact_info = models.CharField(max_length=255)
    test_type = models.CharField(max_length=255, choices=TEST_TYPE_CHOICES)
    status = models.BooleanField(default=True)
    date = models.DateField()
    time = models.TimeField()

    def __str__(self):
        return f'{self.patient_name} {self.contact_info}'


class LabResult(models.Model):
    STATUS_CHOICE = [
        ('positive', 'Positive'),
        ('negative', 'Negative'),
    ]
    lab = models.OneToOneField('Laboratory', on_delete=models.CASCADE)
    patient = models.ForeignKey(CustomUser, on_delete=models.CASCADE, related_name='lab_result_patient',
                                limit_choices_to={'role': CustomUser.PATIENT})
    status = models.CharField(max_length=50, choices=STATUS_CHOICE, default='positive')
    date = models.DateField(auto_now_add=True)
    time = models.TimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.lab}'
