from rest_framework import serializers

from . import models
from .models import CustomUser, Appointment, Doctor, DoctorResponse, Nurse, NurseBook, LabResult, Laboratory
from rest_framework.exceptions import MethodNotAllowed


class PatientSignupSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = ('username', 'password', 'first_name', 'last_name')

    def create(self, validated_data):
        user = CustomUser.objects.create_patient(**validated_data)
        return user


class AppointmentSerializer(serializers.ModelSerializer):
    status = serializers.BooleanField
    patient = serializers.HiddenField(default=serializers.CurrentUserDefault())
    date = serializers.DateField()
    doctor = serializers.IntegerField
    start_time = serializers.TimeField()
    end_time = serializers.TimeField()

    class Meta:
        model = Appointment
        fields = [
            'id',
            'patient_name',
            'patient_surname',
            'contact_info',
            'date',
            'status',
            'patient',
            'doctor',
            'start_time',
            'end_time'
        ]


class AppointmentsListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Appointment
        fields = "__all__"

    def create(self, validated_data):
        raise MethodNotAllowed("POST")

    def update(self, instance, validated_data):
        raise MethodNotAllowed("PUT")

    def partial_update(self, instance, validated_data):
        raise MethodNotAllowed("PATCH")

    def destroy(self, instance):
        raise MethodNotAllowed("DELETE")


class CustomUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = [
            'id',
            'first_name',
            'last_name',
            'role',
            'username'
        ]


class DoctorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Doctor
        fields = "__all__"


class GetFreeTimeSerializer(serializers.Serializer):
    start_time = serializers.TimeField(format='%H:%M')
    end_time = serializers.TimeField(format='%H:%M')


class DoctorResponseSerializer(serializers.ModelSerializer):
    appointment = AppointmentSerializer(read_only=True)

    class Meta:
        model = DoctorResponse
        fields = ['appointment', 'room', 'date']


class DoctorResponseCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = DoctorResponse
        fields = ['appointment', 'room', 'date']


class AppointmentStatusUpdater(serializers.Serializer):
    status = serializers.BooleanField()

    class Meta:
        model = Appointment
        fields = ['status']


class NurseBookSerializer(serializers.ModelSerializer):
    patient = serializers.HiddenField(default=serializers.CurrentUserDefault())
    patient_name = serializers.CharField()
    contact_info = serializers.CharField()
    date = serializers.DateField()
    time = serializers.TimeField()

    class Meta:
        model = NurseBook
        fields = [
            'patient',
            'patient_name',
            'contact_info',
            'date',
            'time'
        ]


class NurseBookListSerializer(serializers.ModelSerializer):
    class Meta:
        model = NurseBook
        fields = '__all__'


class NurseBookStatusUpdaterSerializer(serializers.Serializer):
    status = serializers.ChoiceField(choices=NurseBook.STATUS_CHOICE)

    def update(self, instance, validated_data):
        instance.status = validated_data.get('status', instance.status)
        instance.save()
        return instance


class LaboratorySerializer(serializers.ModelSerializer):
    patient = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = models.Laboratory
        fields = '__all__'


class LabResultSerializer(serializers.ModelSerializer):
    lab = LaboratorySerializer()

    class Meta:
        model = LabResult
        fields = ['lab', 'status', 'date', 'time']

    def create(self, validated_data):
        lab = validated_data.pop('lab')
        lab_instance = models.Laboratory.objects.get(pk=lab)
        lab_result = LabResult.objects.create(lab=lab_instance, **validated_data)
        return lab_result


class LabResultCreateSerializer(serializers.ModelSerializer):
    lab = serializers.IntegerField()

    class Meta:
        model = LabResult
        fields = ['lab', 'status']

    def create(self, validated_data):
        lab = validated_data.pop('lab')
        lab_instance = models.Laboratory.objects.get(pk=lab)
        lab_result = LabResult.objects.create(lab=lab_instance, **validated_data)
        return lab_result


class LaboratoryStatusSerializer(serializers.ModelSerializer):
    patient = serializers.PrimaryKeyRelatedField(queryset=CustomUser.objects.all())

    class Meta:
        model = Laboratory
        fields = '__all__'
        extra_kwargs = {
            'patient_name': {'required': False},
            'contact_info': {'required': False},
            'test_type': {'required': False},
            'date': {'required': False},
            'time': {'required': False},
        }


class LaboratoryOneSerializer(serializers.ModelSerializer):
    patient = serializers.PrimaryKeyRelatedField(queryset=CustomUser.objects.all())

    class Meta:
        model = Laboratory
        fields = ['id', 'patient']


class LabResultAddSerializer(serializers.ModelSerializer):
    lab = serializers.PrimaryKeyRelatedField(queryset=Laboratory.objects.all())
    patient = serializers.PrimaryKeyRelatedField(queryset=CustomUser.objects.all())

    class Meta:
        model = LabResult
        fields = ['lab', 'patient', 'status', 'date', 'time']


class LabResultByUsernameSerializer(serializers.ModelSerializer):
    class Meta:
        model = LabResult
        fields = '__all__'
