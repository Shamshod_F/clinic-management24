from django.urls import path, include
from .views import PatientSignupAPIView, UserLoginAPIView, UserViewSet, GetUserByUsernameAPIView, AppointmentViewSet, \
    DoctorViewSet, GetFreeTimeAPIView, GetDoctorByTypeSet, AppointmentsActiveView, AppointmentsListAPIView, \
    AppointmentsPassiveView, DoctorAppointmentsAPIView, DoctorResponseAPIView, DoctorResponseListView, \
    AppointmentStatusUpdateAPIView, DoctorActiveAppointmentsAPIView, DoctorPassiveAppointmentsAPIView, NurseBookViewSet, \
    NurseBookListAPIView, NurseBookStatusUpdateAPIView, LaboratoryListCreateAPIView, \
    LaboratoryRetrieveUpdateDestroyAPIView, LabResultListCreateAPIView, LabResultViewSet, LabResultViewrReadOnlySet
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'users', UserViewSet, basename='user')
router.register(r'appointments', AppointmentViewSet, basename='appointments')
router.register(r'doctors', DoctorViewSet, basename='doctors')
router.register(r'nurse-book', NurseBookViewSet, basename='nurse-book')
router.register(r'lab-result-new', LabResultViewSet, basename='lab-result-new')
router.register(r'user-lab-result', LabResultViewrReadOnlySet, basename="user-lab-result")

urlpatterns = [
    path('', include(router.urls)),
    path('patient/signup/', PatientSignupAPIView.as_view(), name='patient-signup'),
    path('login/', UserLoginAPIView.as_view(), name='user-login'),
    path('user/<str:username>/', GetUserByUsernameAPIView.as_view(), name='get_user_by_username'),
    path('get-free-time/<int:doctor>/<str:date>/', GetFreeTimeAPIView.as_view(), name='get_free_time'),
    path('doctors-by-type/<str:doctor_type>/', GetDoctorByTypeSet.as_view(), name='doctors-by-type'),
    path('appointments/active/<str:username>/', AppointmentsActiveView.as_view(), name='appointments-active'),
    path('appointments/passive/<str:username>/', AppointmentsPassiveView.as_view(), name='appointments-active'),
    path('appoinments-all/', AppointmentsListAPIView.as_view(), name='appointments-all'),
    path('appointments/doctor/<str:username>/', DoctorAppointmentsAPIView.as_view(), name='doctor-appointments'),
    path('doctor-response/', DoctorResponseAPIView.as_view(), name='doctor-response'),
    path('doctor-response/patient/<str:username>/', DoctorResponseListView.as_view(), name='doctor-response'),
    path('appointments/<int:pk>/update-status/', AppointmentStatusUpdateAPIView.as_view(), name='appointment-status-update'),
    path('appointments/doctor/<str:username>/active/', DoctorActiveAppointmentsAPIView.as_view(), name='doctor-active-appointments'),
    path('appointments/doctor/<str:username>/passive/', DoctorPassiveAppointmentsAPIView.as_view(), name='doctor-passive-appointments'),
    path('nurse-book-list/<str:status>/', NurseBookListAPIView.as_view(), name='nurse-book-list'),
    path('nurse-book/update/<int:pk>/', NurseBookStatusUpdateAPIView.as_view(), name='nursebook-update'),
    path('laboratories/', LaboratoryListCreateAPIView.as_view(), name='lab-list-create'),
    path('laboratories/<int:pk>/', LaboratoryRetrieveUpdateDestroyAPIView.as_view(), name='lab-detail'),
    path('lab-results/', LabResultListCreateAPIView.as_view(), name='lab-result-list-create'),
]
