from rest_framework import status, viewsets, generics, permissions
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.tokens import RefreshToken
from django.contrib.auth import authenticate, login as django_login
from django.http import Http404
from .models import CustomUser, Appointment, Doctor, DoctorResponse, Nurse, NurseBook, Laboratory, LabResult
from .serializers import PatientSignupSerializer, CustomUserSerializer, AppointmentSerializer, DoctorSerializer, \
    GetFreeTimeSerializer, AppointmentsListSerializer, DoctorResponseCreateSerializer, AppointmentStatusUpdater, \
    DoctorResponseSerializer, NurseBookSerializer, NurseBookListSerializer, \
    NurseBookStatusUpdaterSerializer, LaboratorySerializer, LabResultSerializer, \
    LabResultAddSerializer, LaboratoryStatusSerializer
from rest_framework.permissions import AllowAny, IsAuthenticated
from datetime import datetime


class PatientSignupAPIView(APIView):
    permission_classes = [AllowAny]

    def post(self, request):
        """
        Register a new patient.
        """
        serializer = PatientSignupSerializer(data=request.data)
        if serializer.is_valid():
            user = serializer.save()
            refresh = RefreshToken.for_user(user)
            return Response({
                'refresh': str(refresh),
                'access': str(refresh.access_token),
            }, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UserLoginAPIView(APIView):
    permission_classes = [AllowAny]

    def post(self, request):
        username = request.data.get('username')
        password = request.data.get('password')

        user = authenticate(request, username=username, password=password)
        if user is not None:
            django_login(request, user)
            refresh = RefreshToken.for_user(user)
            return Response({
                'refresh': str(refresh),
                'access': str(refresh.access_token),
            }, status=status.HTTP_200_OK)
        else:
            return Response({'error': 'Invalid credentials'}, status=status.HTTP_401_UNAUTHORIZED)


class UserViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = CustomUser.objects.all()
    serializer_class = CustomUserSerializer


class GetUserByUsernameAPIView(APIView):
    permission_classes = [AllowAny]

    def get(self, request, username):
        try:
            user = CustomUser.objects.get(username=username)
            serializer = CustomUserSerializer(user)
            return Response(serializer.data)
        except CustomUser.DoesNotExist:
            return Response({'error': 'User not found'}, status=status.HTTP_404_NOT_FOUND)


class AppointmentViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    serializer_class = AppointmentSerializer

    def get_queryset(self):
        return Appointment.objects.filter(patient=self.request.user)


class AppointmentsListAPIView(ListAPIView):
    permission_classes = [AllowAny]
    serializer_class = AppointmentsListSerializer

    queryset = Appointment.objects.all()


class AppointmentsActiveView(APIView):
    permission_classes = [AllowAny]

    def get(self, request, username):
        try:
            user = CustomUser.objects.get(username=username)
            appointments = Appointment.objects.filter(patient=user, status=True)
            serializer = AppointmentsListSerializer(appointments, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        except CustomUser.DoesNotExist:
            return Response({"detail": "User not found."}, status=status.HTTP_404_NOT_FOUND)


class AppointmentsPassiveView(APIView):
    permission_classes = [AllowAny]

    def get(self, request, username):
        try:
            user = CustomUser.objects.get(username=username)
            appointments = Appointment.objects.filter(patient=user, status=False)
            serializer = AppointmentsListSerializer(appointments, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        except CustomUser.DoesNotExist:
            return Response({"detail": "User not found."}, status=status.HTTP_404_NOT_FOUND)


class DoctorViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Doctor.objects.all()
    serializer_class = DoctorSerializer


class GetFreeTimeAPIView(generics.ListAPIView):
    permission_classes = [AllowAny]
    serializer_class = GetFreeTimeSerializer

    @property
    def get_queryset(self):
        doctor = self.kwargs['doctor']
        date = datetime.strptime(self.kwargs['date'], '%Y-%m-%d').date()

        appointments = Appointment.objects.filter(doctor=doctor, date=date)

        start_time = datetime.strptime('09:00', '%H:%M').time()
        end_time = datetime.strptime('18:00', '%H:%M').time()
        time_slots = []
        current_time = start_time
        while current_time < end_time:
            conflicts = any(appointment.start_time <= current_time <= appointment.end_time
                            or appointment.start_time <= current_time.replace(
                hour=current_time.hour + 1) <= appointment.end_time
                            for appointment in appointments)
            if not conflicts:
                time_slots.append({
                    'start_time': current_time.strftime('%H:%M'),
                    'end_time': (current_time.replace(hour=current_time.hour + 1)).strftime('%H:%M')
                })
            current_time = current_time.replace(hour=current_time.hour + 1)

        return time_slots

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class GetDoctorByTypeSet(APIView):
    permission_classes = [AllowAny]

    def get(self, request, doctor_type):
        doctors = Doctor.objects.filter(doctor_type=doctor_type)
        serializer = DoctorSerializer(doctors, many=True)
        return Response(serializer.data)


class DoctorAppointmentsAPIView(APIView):
    permission_classes = [AllowAny]

    def get(self, request, username):
        try:
            doctor_user = CustomUser.objects.get(username=username, role=CustomUser.DOCTOR)
            doctor = doctor_user.doctor
            appointments = Appointment.objects.filter(doctor=doctor)
            serializer = AppointmentSerializer(appointments, many=True)
            return Response(serializer.data)
        except CustomUser.DoesNotExist:
            raise Http404("Doctor does not exist")
        except Appointment.DoesNotExist:
            return Response([], status=status.HTTP_204_NO_CONTENT)


class DoctorResponseAPIView(generics.CreateAPIView):
    serializer_class = DoctorResponseCreateSerializer

    def post(self, request, *args, **kwargs):
        data = request.data.copy()
        serializer = self.get_serializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DoctorResponseListView(generics.ListAPIView):
    serializer_class = DoctorResponseSerializer

    def get_queryset(self):
        username = self.kwargs['username']
        return DoctorResponse.objects.filter(appointment__patient__username=username)


class AppointmentStatusUpdateAPIView(generics.UpdateAPIView):
    queryset = Appointment.objects.all()
    serializer_class = AppointmentStatusUpdater

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.status = False
        instance.save()
        serializer = self.get_serializer(instance)
        return Response(serializer.data)


class DoctorActiveAppointmentsAPIView(generics.ListAPIView):
    serializer_class = AppointmentSerializer

    def get_queryset(self):
        username = self.kwargs['username']
        return Appointment.objects.filter(doctor__user__username=username, status=True)


class DoctorPassiveAppointmentsAPIView(generics.ListAPIView):
    serializer_class = AppointmentSerializer

    def get_queryset(self):
        username = self.kwargs['username']
        return Appointment.objects.filter(doctor__user__username=username, status=False)


class NurseBookViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    serializer_class = NurseBookSerializer


class NurseBookListAPIView(generics.ListAPIView):
    permission_classes = [AllowAny]
    serializer_class = NurseBookListSerializer

    def get_queryset(self):
        status = self.kwargs.get('status')
        return NurseBook.objects.filter(status=status)


class NurseBookStatusUpdateAPIView(generics.UpdateAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = NurseBookStatusUpdaterSerializer
    queryset = NurseBook.objects.all()


class LaboratoryListCreateAPIView(generics.ListCreateAPIView):
    queryset = Laboratory.objects.all()
    serializer_class = LaboratorySerializer
    permission_classes = [permissions.IsAuthenticated]

    def perform_create(self, serializer):
        serializer.save(patient=self.request.user)


class LaboratoryRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Laboratory.objects.all()
    serializer_class = LaboratoryStatusSerializer
    permission_classes = [permissions.IsAuthenticated]

    def put(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', True)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)

    def patch(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)


class LabResultListCreateAPIView(generics.ListCreateAPIView):
    queryset = LabResult.objects.all()
    serializer_class = LabResultSerializer
    permission_classes = [permissions.IsAuthenticated]

    def perform_create(self, serializer):
        serializer.save(patient=self.request.user)


class LabResultViewSet(viewsets.ModelViewSet):
    queryset = LabResult.objects.all()
    serializer_class = LabResultAddSerializer

    def create(self, request, *args, **kwargs):
        lab_id = request.data.get('lab')
        laboratory_instance = Laboratory.objects.get(pk=lab_id)
        patient_instance = laboratory_instance.patient
        request.data['patient'] = patient_instance.id
        print(request.data['patient'])
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status=status.HTTP_201_CREATED)


class LabResultViewrReadOnlySet(viewsets.ReadOnlyModelViewSet):
    serializer_class = LabResultAddSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        # Filter LabResult instances based on the authenticated user's ID
        return LabResult.objects.filter(patient=self.request.user)